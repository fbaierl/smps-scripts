# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import os
import random


# import matplotlib.pyplot as plt

def psDateStr2MiliSeconds(x):
    t = x.split(':')
    # days, hours and minutes
    if len(t) is 4:
        ms = float(t[0]) * 24 * 60 * 60 * 1000 + float(t[1]) * 60 * 60 * 1000 + float(t[2]) * 60 * 1000
    elif len(t) is 3:
        ms = float(t[0]) * 60 * 60 * 1000 + float(t[1]) * 60 * 1000
    elif len(t) is 2:
        ms = float(t[0]) * 60 * 1000
    elif len(t) is 1:
        ms = 0
    # seconds and miliseconds
    s = t[len(t) - 1].split('.')
    ms += float(s[0]) * 1000 + float(s[1])

    return ms


headers = ['﻿Artikelname', 'Durchlaufzeit', 'LagerWarteAnteil', 'LagerWarteZeit', 'LagerzeitAnteil',
               'ProdArbeitsAnteil', 'ProdArbeitsZeit', 'ProdStörAnteil', 'ProdStörZeit', 'ProduktionszeitAnteil',
               'ProdWarteAnteil', 'ProdWarteZeit', 'WartezeitAnteil', 'BearbzeitAnteil', 'GestörtzeitAnteil',
               'EintrittszeitConWIP', 'Erzeugungszeit']

def read_file(dir_path, file_name):
    # Reading the dataset in a dataframe using Pandas
    
    file_path = dir_path + "/" + file_name

    print('reading file: ', file_path)

    df = pd.read_csv(file_path, sep=";", names=headers, header=1)

    df['Durchlaufzeit'] = df['Durchlaufzeit'].apply(psDateStr2MiliSeconds)
    df['LagerWarteZeit'] = df['LagerWarteZeit'].apply(psDateStr2MiliSeconds)
    df['ProdArbeitsZeit'] = df['ProdArbeitsZeit'].apply(psDateStr2MiliSeconds)
    df['ProdStörZeit'] = df['ProdStörZeit'].apply(psDateStr2MiliSeconds)
    df['ProdWarteZeit'] = df['ProdWarteZeit'].apply(psDateStr2MiliSeconds)
    df['EintrittszeitConWIP'] = df['EintrittszeitConWIP'].apply(psDateStr2MiliSeconds)
    df['Erzeugungszeit'] = df['Erzeugungszeit'].apply(psDateStr2MiliSeconds)

    return df


input_data_dir_path = os.getcwd() + "/input_data/"
input_files_list = os.listdir(input_data_dir_path)




# describe and save file 
for input_file in input_files_list:
	# get data
    raw_df = read_file(input_data_dir_path, input_file)
    # describe & save
    #result_df = raw_df.describe()
    #result_df = result_df.append(pd.Series(raw_df.sum(axis=1), name='total'))
    #result_df.to_csv('output_data/' + input_file.split('.')[0] + '_out.csv')
    
    #headers.remove('﻿Artikelname') # doesnt make sense to compare 

    for header in headers:
	    compare_df = pd.DataFrame(columns=['total', 'mean', 'min', 'max'])
	    # todo iterate over all headers    
	    row_header = input_file.replace('Zeiten_','').replace('.txt','')
	    compare_df.loc[row_header] = [raw_df[header].sum(), raw_df[header].mean(), raw_df[header].min(), raw_df[header].max()]
	    compare_df.to_csv('output_data/comparisons/' + header + '_comp.csv')
	    #print compare_df
   

