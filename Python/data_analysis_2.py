# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import os
import random


def psDateStr2MiliSeconds(x):
       
    ms = 0
    if isinstance(x, float):
        x = '%.3f' % x

    t = x.split(':')
    # days, hours and minutes
    if len(t) is 4:
        ms = float(t[0]) * 24 * 60 * 60 * 1000 + float(t[1]) * 60 * 60 * 1000 + float(t[2]) * 60 * 1000
    elif len(t) is 3:
        ms = float(t[0]) * 60 * 60 * 1000 + float(t[1]) * 60 * 1000
    elif len(t) is 2:
        ms = float(t[0]) * 60 * 1000
    elif len(t) is 1:
        ms = 0

    # seconds and miliseconds
    s = t[len(t) - 1].split('.')
    ms += float(s[0]) * 1000 + float(s[1])

    return ms


headers = ['﻿Artikelname', 'Durchlaufzeit', 'LagerWarteAnteil', 'LagerWarteZeit', 'LagerzeitAnteil',
               'ProdArbeitsAnteil', 'ProdArbeitsZeit', 'ProdStörAnteil', 'ProdStörZeit', 'ProduktionszeitAnteil',
               'ProdWarteAnteil', 'ProdWarteZeit', 'WartezeitAnteil', 'BearbzeitAnteil', 'GestörtzeitAnteil',
               'EintrittszeitConWIP', 'Erzeugungszeit', 'Endzeit', 'Gesamtzeit', 'ConWIPZeit']

def read_file(dir_path, file_name):
    # Reading the dataset in a dataframe using Pandas
    
    file_path = dir_path + "/" + file_name

    df = pd.read_csv(file_path, sep=";", names=headers, header=1)

    df['Durchlaufzeit'] = df['Durchlaufzeit'].apply(psDateStr2MiliSeconds)
    df['LagerWarteZeit'] = df['LagerWarteZeit'].apply(psDateStr2MiliSeconds)
    df['ProdArbeitsZeit'] = df['ProdArbeitsZeit'].apply(psDateStr2MiliSeconds)
    df['ProdStörZeit'] = df['ProdStörZeit'].apply(psDateStr2MiliSeconds)
    df['ProdWarteZeit'] = df['ProdWarteZeit'].apply(psDateStr2MiliSeconds)
    df['EintrittszeitConWIP'] = df['EintrittszeitConWIP'].apply(psDateStr2MiliSeconds)
    df['Erzeugungszeit'] = df['Erzeugungszeit'].apply(psDateStr2MiliSeconds)
    df['Endzeit'] = df['Endzeit'].apply(psDateStr2MiliSeconds)
    df['Gesamtzeit'] = df['Gesamtzeit'].apply(psDateStr2MiliSeconds)
    df['ConWIPZeit']= df['ConWIPZeit'].apply(psDateStr2MiliSeconds)

    return df

def calc_custom_total_time(df):
    df['Durchlaufzeit'] = df['LagerWarteZeit'] + df['ProdArbeitsZeit'] + df['ProdStörZeit'] + df['ProdWarteZeit'] +  df['Erzeugungszeit']
    return df


input_data_dir_path = os.getcwd() + "/input_data"
input_files_list = os.listdir(input_data_dir_path)

# remove irrelevant columns
headers.remove('﻿Artikelname')

for header in headers:

    # create the file to print the result in (one per header)
    compare_df = pd.DataFrame()

    for input_file in input_files_list:
    	print header, input_file
        row_header = input_file.replace('Zeiten_','').replace('.txt','')
        file_path = 'output_data/comparisons/' + header + '_comp.csv'

        # get data
        raw_df = read_file(input_data_dir_path, input_file)

        # calculate the total time
        raw_df = calc_custom_total_time(raw_df)

        # analyse and write data to file
        compare_df[row_header] = [raw_df[header].sum(), raw_df[header].mean(), raw_df[header].min(), raw_df[header].max()]

    # rename the index
    compare_df = compare_df.rename(index={0: 'total', 1: 'mean', 2: 'min', 3: 'max'})

    # save the file
    compare_df.to_csv(file_path)
