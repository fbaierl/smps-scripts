package com.company;

public class WorkStep {

    private int _stepCount;
    private String _resource;
    private float _setupTime;
    private float _timePerPiece;
    private float _humanMachineFactor;

    public WorkStep(){}

    public void setStepCount(int stepCount) {
        this._stepCount = stepCount;
    }

    public int getStepCount() {
        return _stepCount;
    }

    public String getResource() {
        return _resource;
    }

    public void setResource(String resource) {
        this._resource = resource;
    }

    public float getSetupTime() {
        return _setupTime;
    }

    public void setSetupTime(float setupTime) {
        this._setupTime = setupTime;
    }

    public float getTimePerPiece() {
        return _timePerPiece;
    }

    public void setTimePerPiece(float timePerPiece) {
        this._timePerPiece = timePerPiece;
    }

    public float getHumanMachineFactor() {
        return _humanMachineFactor;
    }

    public void setHumanMachineFactor(float humanMachineFactor) {
        this._humanMachineFactor = humanMachineFactor;
    }

}
