package com.company;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    Main program = new Main();
        program.readData();
        program.writeData();
    }

    private void writeData() {

        try {
            String blueprint = readFile("input_data/import_blueprint.xml", Charset.defaultCharset());

            blueprint = blueprint.replace("$$be$$", createObjectXMLString());
            blueprint = blueprint.replace("$$count$$", createCountXMLString());
            blueprint = blueprint.replace("$$name$$", createNameXMLString());
            blueprint = blueprint.replace("$$attribute_table$$", createAttributeTableXMLString());

            PrintWriter out = new PrintWriter("output/result.xml");
            out.print(blueprint);

            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String createAttributeTableXMLString() {

        String result = "";

        try {
            String blueprint = readFile("input_data/attribute_table_blueprint.xml", Charset.defaultCharset());
            
            for(Product product : Product.allProducts){
                String partialResult = blueprint;

                partialResult = partialResult.replace("$$attribute_table_name$$", product.getArticleNumber());
                partialResult = partialResult.replace("$$working_steps$$", createWorkingStepsXML(product));
                partialResult = partialResult.replace("$$resources$$", createResourcesXML(product));
                partialResult = partialResult.replace("$$setup_time$$", createSetupTimeXML(product));
                partialResult = partialResult.replace("$$time_per_piece$$", createTimePerPieceXML(product));
                partialResult = partialResult.replace("$$human_machine_factor$$", createHumanMachineFactorXML(product));
                
                result += partialResult + "\n";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.substring(0, result.length()-1);
    }

    private CharSequence createWorkingStepsXML(Product product) {

        String result = "";

        for(WorkStep step : product.getWorkSteps()){
            result += "\t\t\t\t<Cell>" + step.getStepCount() + "</Cell>\n";
        }

        return result.substring(0, result.length()-1);
    }

    private String createHumanMachineFactorXML(Product product) {

        String result = "";

        for(WorkStep step : product.getWorkSteps()){
            result += "\t\t\t\t<Cell>" + step.getHumanMachineFactor() + "</Cell>\n";
        }
        return result.substring(0, result.length()-1);
    }

    private String createTimePerPieceXML(Product product) {
        String result = "";

        for(WorkStep step : product.getWorkSteps()){
            result += "\t\t\t\t<Cell>" + toPSTimeFormat(step.getTimePerPiece()) + "</Cell>\n";
        }
        return result.substring(0, result.length()-1);
    }

    private String toPSTimeFormat(float minutes) {
        // format: 00:00.0000, where m:ss:msmsmsms
        int m = (int) minutes;
        String result = m > 10 ? m +"": "0" + m;
        result =  result  + ":00.0000";

        return result;
    }

    private String createSetupTimeXML(Product product) {
        String result = "";

        for(WorkStep step : product.getWorkSteps()){
            result += "\t\t\t\t<Cell>" + toPSTimeFormat(step.getSetupTime()) + "</Cell>\n";
        }
        return result.substring(0, result.length()-1);
    }

    private String createResourcesXML(Product product) {
        String result = "";

        for(WorkStep step : product.getWorkSteps()){
            result += "\t\t\t\t<Cell>" + step.getResource() + "</Cell>\n";
        }
        return result.substring(0, result.length()-1);
    }

    private String createNameXMLString() {
        String result = "";

        for(Product product : Product.allProducts){
            result += "\t\t\t<Cell>" + product.getArticleNumber() + "</Cell>\n";
        }

        return result.substring(0, result.length()-1);
    }

    private String createCountXMLString() {
        String result = "";

        for(Product product : Product.allProducts){
            result += "\t\t\t<Cell>1</Cell>\n";
        }

        return result.substring(0, result.length()-1);
    }

    private String createObjectXMLString() {
        String result = "";

        for(Product product : Product.allProducts){
            result += "\t\t\t<Cell>.BEs.Produkt</Cell>\n";
        }

        return result.substring(0, result.length()-1);
    }

    public void readData(){
        try (BufferedReader br = new BufferedReader(new FileReader("input_data/standardarbeitsplaene.csv"))) {
            String line;
            String currentArticleNumber = null;
            String previousArticleNumber = null;
            Product currentProduct = null;

            while ((line = br.readLine()) != null) {

                List<String> lineList = Arrays.asList(line.split(";"));

                currentArticleNumber = lineList.get(0);

                if(currentArticleNumber == null || !currentArticleNumber.equals(previousArticleNumber)){
                    currentProduct = new Product(currentArticleNumber);
                }

                WorkStep step = new WorkStep();
                step.setStepCount(Integer.parseInt(lineList.get(2)));
                step.setResource(lineList.get(4));
                step.setSetupTime(Float.parseFloat(lineList.get(5).replace(",",".")));
                step.setTimePerPiece(Float.parseFloat(lineList.get(6).replace(",",".")));
                step.setHumanMachineFactor(Float.parseFloat(lineList.get(7).replace(",",".")));
                currentProduct.AddStep(step);

                previousArticleNumber = currentArticleNumber;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
    
}
