package com.company;

import java.util.ArrayList;

public class Product {

    private String _articleNumber;
    private ArrayList<WorkStep> _steps = new ArrayList<>();

    public static ArrayList<Product> allProducts = new ArrayList<>();

    public Product(String articleNumber){
        _articleNumber = articleNumber;

        allProducts.add(this);
    }

    public void AddStep(WorkStep step){
        _steps.add(step);
    }

    public String getArticleNumber() {
        return _articleNumber;
    }

    public ArrayList<WorkStep> getWorkSteps() {
        return _steps;
    }
}
